﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            /// Bloc d'entrée de valeurs des variables " a, b "

            int a = 15; /// Valeur de la variable a qui vaut "15"
            int b = 20;/// Valeur de la variable b qui vaut "20"
            int somme = (a + b); /// Valeur somme de a et b

            /// bloc d'affichage des valeurs entrées précédement 

            Console.WriteLine("La variable a vaut : " + a); /// Affichage de la variable a
            Console.WriteLine("La variable b vaut : " + b); /// Affichage de la variable b
            Console.WriteLine("La somme des deux variable a et b vaut : " + somme);  /// affichage de la somme des variables a et b
            Console.ReadLine(); /// attente d'une réaction de l'utilisateur (Pause)
        }
    }
}
